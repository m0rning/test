var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subscriberSchema = new Schema({
  categories: Array,
  city: String,
  salary: String,
  email: String
});

module.exports = mongoose.model('Subscriber', subscriberSchema);
