var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var distributerSchema = new Schema({
  category: String,
  city: String,
  salary: String,
  companyName: String,
  vacancyTitle: String,
  vacancyDescription: String,
  email: String
});

module.exports = mongoose.model('Distributer', distributerSchema);
