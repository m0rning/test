'use strict';

var express = require('express');
var router = express.Router();
var Subscriber = require('./models/subscriber.model.js');
var Distributer = require('./models/distributer.model.js');

module.exports = function(app) {
  // Insert routes below
  app.use('/api/subscribe', require('./api/subscribe'));
  app.use('/api/distribute', require('./api/distribute'));
  app.use('/api/admin', require('./api/admin'));

  // All undefined asset or api routes should return a 404
  // app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    // .get(errors[404]);

}
