'use strict';

var express = require('express');
var controller = require('./distribute.controller');

var router = express.Router();

router.get('/', controller.index);
router.post('/post', controller.create);

module.exports = router;
