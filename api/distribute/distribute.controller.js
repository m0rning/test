var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var options = {
    auth: {
        api_key: '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'
    }
}
var Promise = require('bluebird');
var transporter = Promise.promisifyAll(nodemailer.createTransport(sgTransport(options)));
var Subscriber = require('../../models/subscriber.model.js');
var Distributer = require('../../models/distributer.model.js');

exports.index = function(req, res) {
  Distributer.find().then(function(distributors) {
    res.json(distributors);
  }).catch(function(err) {
    res.json(500, err);
  });
};


exports.create = function(req, res) {
  var distr;
  Distributer.create(req.body)
  .then(function(distributer) {
    distr = distributer;
    return Subscriber.find({
      categories: distributer.category,
      city: distributer.city,
      salary: distributer.salary
    });
  })
  .then(function(subscribers) {
    return Promise.each(subscribers, function(subscriber) {
      var mailOptions = {
          from: distr.email,
          to: subscriber.email, // list of receivers
          subject: distr.category + '[' + distr.city + ']' + '[' + distr.salary + ']', // Subject line
          text: 'Hello world', // plaintext body
          html: distr.vacancyDescription // html body
      };
      return transporter.sendMailAsync(mailOptions)
      .then(function(info) {
        console.log('sended email', info);
      });
    });
  })
  .then(function() {
    res.send(200);
  })
  .catch(function(err) {
    console.error(err);
    res.json(500, err);
  });
};
