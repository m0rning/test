var Promise = require('bluebird');
var Subscriber = require('../../models/subscriber.model.js');
var Distributer = require('../../models/distributer.model.js');


exports.index = function(req, res, next) {
  var distrs;
  Distributer.find()
  .then(function(distributers) {
    distrs = distributers;
    return Subscriber.find();
  })
  .then(function(subscribers) {
    res.json({ distributers: distrs, subscribers: subscribers});
  })
  .catch(function(err) {
    console.error(err);
    res.json(500, err);
  });

};
