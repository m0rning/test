var Promise = require('bluebird');
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var options = {
    auth: {
        api_key: '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'
    }
}
var transporter = Promise.promisifyAll(nodemailer.createTransport(sgTransport(options)));
var Subscriber = require('../../models/subscriber.model.js');
var Distributer = require('../../models/distributer.model.js');



exports.index = function(req, res) {
  Subscriber.find().then(function(subscribers) {
    res.json(subscribers);
  }).catch(function(err) {
    res.json(500, err);
  });
};


exports.apply = function(req, res) {
  var distr;
  Subscriber.create(req.body)
  .then(function(subscriber) {
    subscr = subscriber;
    return Distributer.find({
      category: { $in: subscriber.categories},
      city: subscriber.city,
      salary: subscriber.salary
    });
  })
  .then(function(distributers) {
    return Promise.each(distributers, function(distributer) {
      var mailOptions = {
          from: distributer.email,
          to: subscr.email, // list of receivers
          subject: distributer.category + '[' + distributer.city + ']' + '[' + distributer.salary + ']', // Subject line
          text: 'Hello world', // plaintext body
          HtmlBody: distributer.vacancyDescription // html body
      };
      return transporter.sendMailAsync(mailOptions)
      .then(function(info) {
        console.log('sended email', info);
      });
    });
  })
  .then(function() {
    res.send(200);
  })
  .catch(function(err) {
    console.error(err);
    res.json(500, err);
  });
};
