(function() {
  'use strict';

  config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider'];

  function config($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });


    $stateProvider
        .state('home', {
          url: '/',
          templateUrl: 'app/views/home.html',
          controller: 'MainController',
          controllerAs: 'main',
        })
        .state('admin', {
          url: '/admin',
          templateUrl: 'app/views/admin.html',
          controller: 'AdminController',
          controllerAs: 'admin',
        });
    $urlRouterProvider.otherwise('/');
  }

  angular
    .module('app', [
            'ui.router',
            'ngResource',
            'ui.select2'
      ])
    .factory('config', config)
    .config(config)
    .run()

})();
