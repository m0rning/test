(function(){
  'use strict';

  angular
    .module('app')
    .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$state', '$rootScope', 'Subscriber', 'Distributer'];

    function MainController($scope, $state, $rootScope, Subscriber, Distributer) {
      var vm = this;
      vm.errorMessage = '';
      vm.categories = [
        {
          id: 1,
          text: 'HR manager'
        },
        {
          id: 2,
          text: 'JS developer'
        },
        {
          id: 3,
          text: 'Ruby developer'
        },
        {
          id: 4,
          text: 'PHP developer'
        },
        {
          id: 5,
          text: 'Python developer'
        }
      ];
      vm.cities = [
        {
          id: 1,
          text: 'Munich'
        },
        {
          id: 2,
          text: 'Frankfurt'
        },
        {
          id: 3,
          text: 'Dusseldorf'
        },
        {
          id: 4,
          text: 'Hamburg'
        },
        {
          id: 5,
          text: 'Heidelberg'
        }
      ];
      vm.salary = [
        {
          id: 1,
          text: 'up to 200 000'
        },
        {
          id: 2,
          text: '201 000 - 400 000'
        },
        {
          id: 3,
          text: '401 000 - 600 000'
        }
      ];
      vm.subscriber = {
        categories: null,
        city: null,
        salary: null,
        email: null
      };
      vm.distributer = {
        category: null,
        city: null,
        salary: null,
        email: null,
      };

      vm.validVacancy = validVacancy;
      vm.makeSubscribe = makeSubscribe;
      vm.publishVacancy = publishVacancy;

      function makeSubscribe() {
        for (var field in vm.subscriber) {
          if (Array.isArray(vm.subscriber[field]) && vm.subscriber[field].length < 1) {
              return vm.subscriberErorMessage = field + ' is required!';
          } else if (vm.subscriber[field] === null) {
            return vm.subscriberErorMessage = field + ' is required!';
          }
        }
        Subscriber.makeSubscribe({}, vm.subscriber, function(response) {

          // if(response.status.success) {
            vm.subscriberErorMessage = '';
            // vm.subscriber = {
            //   categories: null,
            //   city: null,
            //   salary: null,
            //   email: null
            // };
          // }
          $state.reload();
        });
      };

      function validVacancy() {
        for (var field in vm.distributer) {
          if (vm.distributer[field] === null) {
            console.log(field);
            return vm.errorMessage = field + ' is required!';
          }
        }
        vm.distributer.companyName = null;
        vm.distributer.vacancyTitle = null;
        vm.distributer.vacancyDescription = null;
        vm.errorMessage = '';
        $('#myModal').modal('show');
      }

      function publishVacancy() {

        for (var field in vm.distributer) {
          if (vm.distributer[field] === null) {
            console.log(field);
            return vm.errorMessage = field + ' is required!';
          }
        }

        Distributer.publishVacancy({}, vm.distributer, function(response) {
          // if(response.status.success) {
            // vm.distributer = {
            //   category: null,
            //   city: null,
            //   salary: null,
            //   email: null,
            // };
            // $('#myModal').modal('hide');
          // } else {
          //   return vm.errorMessage = 'Incorrect Email';
          // }
          $('#myModal').modal('hide');
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
          $state.reload();
        });
      };

    }
})();
