(function() {
  'use strict';

  angular
    .module('app')
    .controller('AdminController', AdminController);

    AdminController.$inject = ['$scope', '$rootScope', 'Distributer', 'Subscriber'];

    function AdminController($scope, $rootScope, Distributer, Subscriber) {
      var vm = this;
      vm.distributers = Distributer.query();
      vm.subscribers = Subscriber.query();
    }
})();
