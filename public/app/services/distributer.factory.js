angular.module('app').factory('Distributer', function($resource) {
  return $resource('/api/distribute/:controller', {}, {
    publishVacancy: {
      method: 'POST',
      params: {
        controller: 'post'
      }
    }
  });
});
