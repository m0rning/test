angular.module('app').factory('Subscriber', function($resource) {
  return $resource('/api/subscribe/:controller', {}, {
    makeSubscribe: {
      method: 'POST',
      params: {
        controller: 'apply',
      }
    }
  });
});
